//
//  MainDataSource.swift
//  reddit
//
//  Created by Rudy Matos on 11/8/21.
//

import UIKit

class MainDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    private(set) var viewModel: MainViewModel

    var dataSourceDidSelectRow: ((Post, IndexPath) -> ())?
    
    init(viewModel: MainViewModel) {
        self.viewModel = viewModel
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.store.posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? MainCell else {
            preconditionFailure("Invalid Cell")
        }
        let post = viewModel.getPost(at: indexPath)
        let cellViewModel = MainCellViewModel(with: post, and: viewModel.store)
        cell.configure(cellViewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.numberOfRows - 10 {
            viewModel.fetchData(after: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 350
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = viewModel.getPost(at: indexPath)
        dataSourceDidSelectRow?(post, indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
}
