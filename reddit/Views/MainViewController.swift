//
//  ViewController.swift
//  reddit
//
//  Created by Rudy Matos on 11/8/21.
//

import UIKit

class MainViewController: UIViewController {
    
    private(set) var viewModel: MainViewModel
    private(set) var dataSource: MainDataSource
    
    //MARK :- UI Elements
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(MainCell.self, forCellReuseIdentifier: "cell")
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
        return tableView
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        return refreshControl
    }()
    
    init(viewModel: MainViewModel, dataSource: MainDataSource){
        self.viewModel = viewModel
        self.dataSource = dataSource
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    private func configureUI() {
        title = "Reddit Posts"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        dataSource.dataSourceDidSelectRow = { [weak self] post, _ in
            guard let strongSelf = self else { return }
            guard !post.data.url.isEmpty, let url = URL(string: strongSelf.viewModel.format(permanLink: post.data.permalink))  else {
                strongSelf.showInvalidURLAlert()
                return
            }
            UIApplication.shared.open(url)
        }
        
        setupLayout()
        fetchData()
    }
    
    private func setupLayout(){
        self.view.addSubview(tableView)
        tableView.addSubview(refreshControl)
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
    }
    
    private func fetchData() {
        viewModel.didFinishLoadingData = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.tableView.reloadData()
            strongSelf.refreshControl.endRefreshing()

        }
        viewModel.fetchData()
    }
    
    private func showInvalidURLAlert() {
        let controller = UIAlertController(title: "Invalid URL", message: "Cannot access to selected Post", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        controller.addAction(okAction)
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc
    private func refresh(_ sender: UIRefreshControl) {
        viewModel.cleanAllPosts()
        tableView.reloadData()
        viewModel.reloadAll()
    }
}

