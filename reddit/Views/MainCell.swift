//
//  MainCell.swift
//  reddit
//
//  Created by Rudy Matos on 11/8/21.
//

import Foundation
import UIKit

class MainCell: UITableViewCell {
    
    private(set) var viewModel: MainCellViewModel?
    
    private var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        label.numberOfLines = 3
        return label
    }()
    
    private var upVoteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private var downVoteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private var scoreLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .body)
        return label
    }()
    
    private var thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 0
        return stackView
    }()
    
    private var actionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .equalSpacing
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.spacing = 8
        return stackView
    }()
    
    private var votesStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.axis = .horizontal
        stackView.spacing = 8
        return stackView
    }()
    
    private var bottomView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .lightGray
        return view
    }()
    
    func configure(_ viewModel: MainCellViewModel) {
        self.viewModel = viewModel
        configureUI()
    }
    
    private func configureUI() {
        guard let viewModel = viewModel else { return }
        setupLayout()
        titleLabel.text = viewModel.title
        upVoteLabel.text = viewModel.upvotes
        downVoteLabel.text = viewModel.downvotes
        scoreLabel.text = viewModel.score
        viewModel.getImage { [weak self] image in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                strongSelf.thumbnailView.image = image
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbnailView.image = nil
        titleLabel.text = ""
        scoreLabel.text = ""
        upVoteLabel.text = ""
        downVoteLabel.text = ""
    }
    
    private func setupLayout(){
        
        contentView.addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(thumbnailView)
        stackView.addArrangedSubview(bottomView)
        
        bottomView.addSubview(actionStackView)
        votesStackView.addArrangedSubview(upVoteLabel)
        votesStackView.addArrangedSubview(downVoteLabel)
        
        actionStackView.addArrangedSubview(votesStackView)
        actionStackView.addArrangedSubview(scoreLabel)
        
        stackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
        stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        thumbnailView.heightAnchor.constraint(greaterThanOrEqualTo: contentView.heightAnchor, multiplier: 0.50).isActive = true
        thumbnailView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        bottomView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.10).isActive = true
        bottomView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        
        actionStackView.topAnchor.constraint(equalTo: bottomView.topAnchor).isActive = true
        actionStackView.leadingAnchor.constraint(equalTo: bottomView.leadingAnchor, constant: 8).isActive = true
        actionStackView.trailingAnchor.constraint(equalTo: bottomView.trailingAnchor, constant: -8).isActive = true
        actionStackView.bottomAnchor.constraint(equalTo: bottomView.bottomAnchor).isActive = true
        
    }
    
    
}
