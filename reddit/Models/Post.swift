//
//  Post.swift
//  reddit
//
//  Created by Rudy Matos on 11/8/21.
//

import Foundation

struct PostRequest: Codable {
    let kind: String
    let data: PostRequestData
}

struct PostRequestData: Codable {
    let after: String
    let children: [Post]
}

struct Post: Codable {
    let data: PostData
}

struct PostData: Codable {
    
    let authorFullName: String
    let title: String
    let downs: Int
    let thumbnailHeight: Int?
    let name: String
    let upvoteRatio: Double
    let ups: Int
    let thumbnailWidth: Int?
    let thumbnail: String
    let author: String
    let numComments: Int
    let permalink: String
    let url: String
    let score: Int
    let preview: Preview?

    enum CodingKeys: String, CodingKey {
        case authorFullName = "author_fullname"
        case title, downs
        case thumbnailHeight = "thumbnail_height"
        case name
        case upvoteRatio = "upvote_ratio"
        case ups
        case thumbnailWidth = "thumbnail_width"
        case thumbnail, author
        case numComments = "num_comments"
        case permalink, url, score, preview
    }
}

struct Preview: Codable {
    let images: [Image]
}

struct Image: Codable {
    let source: Source
    let resolutions: [Source]
    let id: String
}

struct Source: Codable {
    let url: String
    let width, height: Int
    
    var unscapedURL: URL? {
        let cleanURL = url.replacingOccurrences(of: "amp;", with: "")
        guard let url = URL(string: cleanURL) else { return nil }
        return url
    }
}
