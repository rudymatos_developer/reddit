//
//  MainViewModel.swift
//  reddit
//
//  Created by Rudy Matos on 11/8/21.
//

import Foundation
import Combine
import UIKit

class MainViewModel {
    
    internal var subscriptions = Set<AnyCancellable>()
    
    var didFinishLoadingData: (() -> ())?
    
    var store: MainStore
    
    init(_ store: MainStore){
        self.store = store
        configureSubjects()
    }
    
    var numberOfRows : Int {
        return store.posts.count
    }
    
    func getPost(at index: IndexPath) -> Post{
        return store.posts[index.item]
    }
    
    private func configureSubjects() {
        store.subject.sink { [weak self] post in
            guard let strongSelf = self else { return }
            strongSelf.didFinishLoadingData?()
        }.store(in: &subscriptions)
    }
    
    func format(permanLink: String) -> String {
        return "https://reddit.com\(permanLink)"
        
    }
    
    func cleanAllPosts() {
        store.reloadAll()
    }
    
    func reloadAll(){
        store.reloadAll()
    }
    
    func fetchData() {
        store.fetchDataCombine()
    }
    
    func fetchData(after: Bool) {
        store.fetchDataCombine(after: after)
    }
    
}
