//
//  MainCellViewModel.swift
//  reddit
//
//  Created by Rudy Matos on 11/8/21.
//

import Foundation
import UIKit

class MainCellViewModel {
    
    private(set) var store: MainStore
    private(set) var post: Post
    
    private let numberFormatter : NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        return numberFormatter
    }()
    
    private let BG_IMAGE: UIImage? = {
        return UIImage(named: "bg")
    }()
    
    init(with post: Post, and store: MainStore){
        self.post = post
        self.store = store
    }
    
    var title : String{
        return post.data.title
    }
    
    var upvotes: String {
        return "⬆️: \(getDecimalValue(from: post.data.ups))"
    }
    
    var downvotes: String {
        return "⬇️: \(getDecimalValue(from: post.data.downs))"
    }
    
    var score: String {
        return "🎖: \(getDecimalValue(from: post.data.score))"
    }
    
    func getImage(completion: @escaping ((UIImage?) -> () )) {
        guard let images = post.data.preview?.images,
              let image = images.flatMap({ $0.resolutions }).sorted(by: { $0.width > $1.width}).first,
              let imageURL = image.unscapedURL else {
                  completion(BG_IMAGE)
                  return
              }
        store.fetchImage(imageURL: imageURL, completion: completion)
    }
    
    private func getDecimalValue(from value: Int) -> String {
        guard let number = numberFormatter.string(from: NSNumber(value: value)) else { return "0" }
        return number
    }
    
}
