//
//  MainStore.swift
//  reddit
//
//  Created by Rudy Matos on 11/8/21.
//

import Foundation
import Combine
import UIKit

class MainStore {
    
    private let PERMA_URL_LINK = "https://reddit.com/.json?limit=20"
    
    var posts: [Post] = []
    var subject = PassthroughSubject<[Post], Never>()
    var subscriptions = [AnyCancellable]()
    var latest: String = ""
    
    private let cache = NSCache<NSString, UIImage>()
    private var session = URLSession.shared
    
    func cleanAllPosts(){
        posts.removeAll()
        latest = ""
    }
    
    func reloadAll() {
        cleanAllPosts()
        fetchDataCombine()
    }
    
    func fetchDataCombine(after: Bool = false) {
        let url = PERMA_URL_LINK + (after && !latest.isEmpty ? "&after=\(latest)"  : "")
        if let url = URL(string: url) {
            URLSession.shared.dataTaskPublisher(for: url)
                .map { $0.data }
                .decode(type: PostRequest.self, decoder: JSONDecoder())
                .receive(on: DispatchQueue.main)
                .sink(receiveCompletion: { completion in
                    print(completion)
                }) { [weak self] requestData in
                    guard let strongSelf = self else { return }
                    strongSelf.latest = requestData.data.after
                    strongSelf.posts.append(contentsOf: requestData.data.children)
                    strongSelf.subject.send(requestData.data.children)
                }.store(in: &subscriptions)
        }
    }
    
    func fetchImage(imageURL: URL, completion: @escaping ((UIImage?) -> ())) {
        
        if let image = cache.object(forKey: imageURL.absoluteString as NSString) {
            completion(image)
            return
        }
        
        session.dataTask(with: imageURL) { data, response, error in
            if let error = error {
                print("Request error: ", error)
                completion(nil)
                return
            }
            
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                completion(nil)
                return
            }
            guard let data = data else {
                completion(nil)
                return
            }

            guard let image = UIImage(data: data) else {
                completion(nil)
                return
            }
            self.cache.setObject(image, forKey: imageURL.absoluteString as NSString)
            completion(image)
        }
        .resume()
    }
    
}
