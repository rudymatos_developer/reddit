//
//  redditTests.swift
//  redditTests
//
//  Created by Rudy Matos on 11/8/21.
//

import XCTest
@testable import reddit

class redditTests: XCTestCase {

    private var viewModel : MainCellViewModel!
    
    override func setUp() {
        let store = MockStore()
        viewModel = MainCellViewModel(with: store.mockedPost, and: store)
    }
    
    func testUpVotes() {
        XCTAssertEqual(viewModel.upvotes, "⬆️: 10")
    }
    
    func testDownVotes() {
        XCTAssertEqual(viewModel.downvotes, "⬇️: 10")
    }
    
    func testScore() {
        XCTAssertEqual(viewModel.score, "🎖: 10")
    }
    
    func testTitle() {
        XCTAssertEqual(viewModel.title, "Test")
    }
    
    func testImageIsDefault() {
        viewModel.getImage { image in
            XCTAssertNotNil(image)
        }
    }
    
}

final class MockStore: MainStore {
    
    var mockedPost: Post = {
        let data = PostData(authorFullName: "Test", title: "Test", downs: 10, thumbnailHeight: nil, name: "Test", upvoteRatio: 0, ups: 10, thumbnailWidth: 0, thumbnail: "Test", author: "Test", numComments: 10, permalink: "Test", url: "Test", score: 10, preview: nil)
        let post = Post(data: data)
        return post
    }()
    
}
